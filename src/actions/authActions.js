import firebase from 'firebase';
import { sha256 } from 'react-native-sha256';
import { Actions } from 'react-native-router-flux';
import {
    REGISTER_USER,
    LOGOUT_USER,
    LOGREG_UPDATE,
    LOGREG_LOADING,
    LOGREG_FLOW,
    LOGREG_ERROR,
    LOGREG_CLEAN
} from './types';

export const logregUpdate = ({ prop, value }) => {
    let re;
    switch(prop) {
        case 'email':
            re = /^[a-z0-9._]+@[^!@#$%^&*()+]+\.[a-z]+$/i;
            break;
        case 'password':
            re = /^[^\s]{8,40}$/i;
            break;
        case 'name':
             re = /^[a-z0-9_\.-]{3,20}$/i;
             break;
        case 'gender':
            re = /^(male|female)$/;
            break;
        default:
            re = /.*/i;           
            break;
    }

    return {
        type: LOGREG_UPDATE,
        payload: {
            prop,
            value,
            error: !re.test(value) ? `Invalid ${prop}` : ''
        }    
    }
};

export const handleFlow = (val) => {
    return {
        type: LOGREG_FLOW,
        payload: (val === 'register') ? 'login' : 'register'
    }
}

export const addUser = ({ email, password, name, gender }) => {
    return (dispatch) => {
        dispatch({ type: LOGREG_LOADING });

        sha256(password).then( hash => {
            firebase.auth().createUserWithEmailAndPassword(email, hash)
            .then(user => {
                firebase.database().ref(`users/${user.uid}/data`).set({
                    name, location: '', age: 16, gender, photo: ''
                })
                .then(() => {
                    dispatch({
                        type: REGISTER_USER,
                        payload: user
                    });
                    dispatch({ type: LOGREG_CLEAN });

                    Actions.profileEdit();
                })
            })
            .catch(err => {
                dispatch({
                    type: LOGREG_ERROR,
                    payload: err.message
                })
            })
        })
    }
}

export const loginUser = ({ email, password }) => {
    return (dispatch) => {
        dispatch({ type: LOGREG_LOADING });

        sha256(password).then( hash => {
            firebase.auth().signInWithEmailAndPassword(email, hash)
            .catch(err => {
                dispatch({
                    type: LOGREG_ERROR,
                    payload: err.message 
                });
            });
        });

        firebase.auth().onAuthStateChanged(user => {
            if (user) {
                dispatch({ type: REGISTER_USER,  payload: user });
                dispatch({ type: LOGREG_CLEAN });
                
                Actions.profileEdit();
            } else {
                dispatch({
                    type: LOGREG_ERROR,
                    payload: 'Login Error' 
                });
            }
        });
    };
};

export const logoutUser = () => {
    return (dispatch) => {
        firebase.auth().signOut().then(() => {
            dispatch({ type: LOGREG_CLEAN });
            dispatch({ type: LOGOUT_USER });

            Actions.welcome();
        }).catch(er => {
            dispatch({
                type: LOGREG_ERROR,
                payload: `Error on signing out: ${err.message}` 
            });
        });
    }
}
