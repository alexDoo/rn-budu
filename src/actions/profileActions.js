import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';
import {
    PROFILE_LOAD,
    PROFILE_CHANGE,
    PROFILE_SAVE,
    PROFILE_LOADING,    
    PROFILE_ERROR,
    PROFILE_SIDE_OPEN
} from './types';

export const profileLoad = (uid) => {
    return (dispatch) => {
        firebase.database().ref(`/users/${uid}`)
        .once('value', snapshot => {
            const userData = snapshot.val();
            return dispatch({ 
                type: PROFILE_LOAD, 
                payload: userData.data
            });
        });
    }
}

export const profileChange = ({ prop, value }) => {
    let re;
    switch(prop) {
        case 'name':
             re = /^[a-z0-9_\.-]{3,20}$/i;
             break;
        case 'location':
            re = /^[a-z0-9\/, _\.-]{2,}$/i;
            break;
        case 'photo':
            re = /^(http|https|www)[\.:][^`"'<> ]+\.[a-z]+[^ ]+$/i;
            break;
        case 'age':
            re = /^[0-9]{2}$/;
            if (re.test(value) && (value > 65 || value < 16)) {
                re = /^!!!$/;
            }
            break;
        default:
            re = /.*/i;           
            break;
    }

    return {
        type: PROFILE_CHANGE,
        payload: {
            prop,
            value,
            error: !re.test(value) ? `Invalid ${prop}` : ''
        }
    }
};

export const profileSave = ({ name, location, photo, age, gender, currentUser }) => {
    if (name.length < 3 || location.length < 2 || photo.length < 10 || age < 16 || age > 65) {
        return ({
            type: PROFILE_ERROR,
            payload: 'All fields are mandatory'
        })
    }

    return (dispatch) => {
        dispatch({ type: PROFILE_LOADING })

        firebase.database().ref(`users/${currentUser.uid}/data`).update({
            name, location, photo, age
        })
        .then(() => {
            dispatch({
                type: PROFILE_SAVE,
                payload: { name, location, photo, age, gender }
            });

            Actions.profileView();
        })
        .catch(err => {
            dispatch({
                type: PROFILE_ERROR,
                payload: err.message
            })
        })
    }
}

export const sideToggle = (open) => {
    return ({
        type: PROFILE_SIDE_OPEN,
        payload: !open
    })
}
