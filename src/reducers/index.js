import { combineReducers } from 'redux';
import userReducer from './userReducer';
import authReducer from './authReducer';
import profileReducer from './profileReducer';

export default combineReducers({
  auth: authReducer,
  user: userReducer,
  profile: profileReducer
});
