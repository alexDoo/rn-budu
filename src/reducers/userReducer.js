import {
  REGISTER_USER,
  LOGOUT_USER
} from '../actions/types';

const iniState = {
    currentUser: false,
    users: false
}

export default (state = iniState, action) => {
  switch (action.type) {
    case REGISTER_USER:
      return { ...state, currentUser: action.payload };
    case LOGOUT_USER:
      return iniState;  
    default:
      return state;
  }
};
