import {
    LOGREG_UPDATE,
    LOGREG_LOADING,
    LOGREG_FLOW,
    LOGREG_ERROR,
    LOGREG_CLEAN
} from '../actions/types';

const INITIAL_STATE = {
  email: '',
  password: '',
  name: '',
  gender: '',
  error: {
    email: '',
    password: '',
    name: '',
    gender: ''
  },
  logreg_error: '',
  loading: false,
  flow: 'register'
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGREG_UPDATE:
        return { 
            ...state, [action.payload.prop]: action.payload.value, 
            error: { ...state.error, [action.payload.prop]: action.payload.error } 
        };
    case LOGREG_FLOW:
        return { 
            ...state, flow: action.payload, error: { ...INITIAL_STATE.error }
        };
    case LOGREG_LOADING:
        return { 
            ...state, loading: true
        };
    case LOGREG_ERROR:
        return { 
            ...state, logreg_error: action.payload
        };
    case LOGREG_CLEAN:
        return INITIAL_STATE;       
   default:
      return state;
  }
};
