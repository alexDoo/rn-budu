import {
    PROFILE_LOAD,
    PROFILE_CHANGE,
    PROFILE_SAVE,
    PROFILE_LOADING,
    PROFILE_ERROR,
    PROFILE_SIDE_OPEN
} from '../actions/types';

const iniState = {
    name: '',
    location: '',
    age: 16,
    photo: '',
    gender: '',
    error: {
        name: '',
        location: '',
        age: '',
        photo: ''
    },
    profile_error: '',
    loading: false,
    sidemenu: false
}

export default (state = iniState, action) => {
  switch (action.type) {
    case PROFILE_LOAD:
    case PROFILE_SAVE:
        return { ...iniState, ...action.payload, profile_error: '' }
    case PROFILE_CHANGE:
        return { 
            ...state, [action.payload.prop]: action.payload.value, 
            profile_error: '',
            error: { ...state.error, [action.payload.prop]: action.payload.error }
        };
    case PROFILE_LOADING:
        return { ...state, loading: true }    
    case PROFILE_ERROR:
        return { ...state, profile_error: action.payload, loading: false }
    case PROFILE_SIDE_OPEN:
        return { ...state, sidemenu: action.payload }       
    default:
        return state;
  }
};
