import React from 'react';
import { Router, Scene, Actions } from 'react-native-router-flux';
import { Icon } from 'react-native-elements';

import Entry from './components/Welcome/Entry';
import ProfileEdit from './components/Profile/profileEdit';
import ProfileView from './components/Profile/profileView';
import Auth from './components/auth_hoc';

const RouterComponent = () => {
    return (
        <Router>
            <Scene key="root" headerMode="float">
                <Scene key="welcome" hideNavBar={true}>
                    <Scene key="entry" component={Entry} hideNavBar={true} />
                </Scene>

                <Scene key="profile" hideNavBar={true}>
                    <Scene 
                        key="profileEdit" 
                        component={Auth(ProfileEdit)} 
                        title="Edit Profile" initial />
                    <Scene 
                        key="profileView"
                        component={Auth(ProfileView)} 
                        title="Profile" />
                </Scene>
            </Scene>
        </Router>
    );
}

export default RouterComponent;
