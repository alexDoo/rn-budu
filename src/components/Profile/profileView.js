import React, { Component } from 'react';
import { ScrollView, View, Text, StyleSheet, Image, Alert } from 'react-native';
import { Avatar, Divider, Card, Text as Heading } from 'react-native-elements';
import { NavigationBar, Title, Subtitle } from '@shoutem/ui';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

class ProfileView extends Component {
    render() {
        return (
            <ScrollView>
                <NavigationBar
                    hasHistory
                    navigateBack={() => Actions.profileEdit()}
                    centerComponent={<Title>{`${this.props.name} Profile`}</Title>}
                />

                <Card containerStyle={[styles.container, styles.card]}>
                    <Avatar
                        xlarge
                        rounded
                        source={{uri: this.props.photo}}
                        containerStyle={styles.avaCss}
                    />
                    <View styleName="content">
                        <Heading h4>{`${this.props.name} Profile`}</Heading>
                        <View style={styles.profileData}>
                            <View style={styles.titleCss}>
                                <Title>{`Age: ${this.props.age}`}</Title>
                            </View>
                            <Divider style={styles.diva} />
                            <View style={styles.titleCss}>
                                <Title>{`Location: ${this.props.location}`}</Title>
                            </View>
                            <Divider style={{ backgroundColor: 'grey' }} />
                            <View style={styles.titleCss}>
                                <Title>{`Gender: ${this.props.gender}`}</Title>
                            </View>
                        </View>
                    </View>
                </Card>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 160,
        flex:1,
        flexDirection: 'column',
        alignItems:'center' 
   },
    card: {
        justifyContent: 'center',
        width: '98%',
        height: 'auto',
        marginLeft: '1%',
        marginRight: '1%'
    },
    avaCss: {
        marginTop: -86,
        marginBottom: 10,
        borderWidth: 4, 
        borderColor: '#066f9c',
        elevation: 20
    },
    profileData: {
        marginTop: 40,
        alignSelf: 'flex-start',
        justifyContent: 'space-between'
    },
    titleCss: {
        marginTop: 10,
        marginBottom: 4,
    },
    diva: {
        backgroundColor: 'grey', 
        marginTop: 4
    }
})

const mapStateToProps = ({ user, profile }) => {
    const { name, location, photo, age, gender, sidemenu } = profile;
    const { currentUser } = user;

    return { name, location, age, photo, gender, sidemenu, currentUser };
}

export default connect(mapStateToProps, {})(ProfileView);
