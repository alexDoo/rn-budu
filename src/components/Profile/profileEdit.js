import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { Card, Slider, FormLabel, FormInput, Button, Icon } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import { NavigationBar, Title } from '@shoutem/ui';
import { connect } from 'react-redux';

import { Spinner } from '../common/Spinner';
import { 
    profileLoad,
    profileChange,
    profileSave,
    logoutUser
} from '../../actions';

class ProfileEdit extends Component {
    componentWillMount() {
        this.props.profileLoad(this.props.currentUser.uid);
    }

    _renderButton() {
        if (!this.props.loading) {
            return (
                <Button
                    raised
                    Component={TouchableOpacity}
                    title="Save Profile"
                    onPress={() => this.userSave()}
                    icon={{ name: 'done' }}
                    backgroundColor="#04dce0"
                />
            )
        }

        return <Spinner size="large" />;
    }

    _renderFormErrors() {
        let err = Object.values(this.props.error).filter(er => {
            if (er && er.length) {
                return er;
            }
        });

        return `${err.join(', ')} ${this.props.profile_error}`;
    }

    _renderLeftNav() {
        return (
            <Button 
                large
                onPress={() => Actions.profileView()}
                icon={{ name: 'person', color: '#000' }}
                buttonStyle={styles.navBtn} />
        )
    }

    _renderRightNav() {
        return (
            <Button 
                large
                onPress={() => this.props.logoutUser()}
                icon={{ name: 'exit-to-app', color: '#000' }} 
                buttonStyle={[styles.navBtn, { justifyContent: 'flex-end' }]} />
        )
    }

    userSave() {
        if (Object.values(this.props.error).join('').length < 1) {
            this.props.profileSave(this.props)
        }

        return false;
    }

    render() {
        return (
            <View>
                <NavigationBar
                    leftComponent={this._renderLeftNav()}
                    centerComponent={<Title>Edit Profile</Title>}
                    rightComponent={this._renderRightNav()}
                />
                <Card containerStyle={[styles.container, styles.card]}>
                    <View style={styles.input}>
                        <FormLabel>Name</FormLabel>
                        <FormInput
                            value={this.props.name}
                            autocorrect={false}
                            containerStyle={styles.containerFI}
                            maxLength={200}
                            onChangeText={value => this.props.profileChange({ prop: 'name', value })}
                            label={'Name'}
                            placeholder="James" />
                    </View>        

                    <View style={styles.input}>
                        <FormLabel>Location</FormLabel>
                        <FormInput
                            value={this.props.location}
                            autocorrect={false}
                            containerStyle={styles.containerFI}
                            maxLength={200}
                            onChangeText={value => this.props.profileChange({ prop: 'location', value })}
                            label={'Location'}
                            placeholder="London" />
                    </View>        

                    <View style={styles.input}>
                        <FormLabel>Photo</FormLabel>
                        <FormInput
                            value={this.props.photo}
                            autocorrect={false}
                            containerStyle={styles.containerFI}
                            maxLength={200}
                            onChangeText={value => this.props.profileChange({ prop: 'photo', value })}
                            label={'Photo'}
                            placeholder="external image URL" />
                    </View>        

                    <View style={[styles.input, { marginTop:26, marginBottom: 20 }]}>
                        <Slider
                            value={this.props.age}
                            style={styles.containerFI}
                            minimumValue={16}
                            maximumValue={65}
                            step={1}
                            onValueChange={value => this.props.profileChange({ prop: 'age', value })} />
                        <Text>Age: {this.props.age}</Text>
                    </View>

                    {this._renderButton()}

                    <View style={styles.erview}>
                        <Text style={styles.ertxt}>{this._renderFormErrors()}</Text>
                    </View>
                </Card>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        color: '#000'
    },
    container: {
      marginTop: 80
   },
    card: {
        width: '98%',
        height: 'auto',
        marginLeft: '1%',
        marginRight: '1%'
    },
    input: {
        alignItems: 'flex-start',
        width: '100%',
        height: 60
    },
    containerFI: {
        width: '92%', 
        marginLeft: '4%'
    },
    erview: {
        width: '92%',
        marginTop: 20,
        marginLeft: '4%',
        height: 'auto',
        minHeight: 40
    },
    ertxt: {
        fontSize: 18,
        color: 'red'
    },
    navBtn: {
        backgroundColor: '#fff', 
        margin: 0, 
        padding: 0
    }
})

const mapStateToProps = ({ user, profile }) => {
    const { name, location, photo, gender, error, profile_error, loading } = profile;
    const age = parseInt(profile.age);
    const { currentUser } = user;

    return { name, location, age, photo, gender, error, profile_error, loading, currentUser };
}

export default connect(mapStateToProps, {
    profileLoad,
    profileChange,
    profileSave,
    logoutUser
})(ProfileEdit);
