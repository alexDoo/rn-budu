import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import RadioButton from 'radio-button-react-native';

const Radio = ({ value, label, txtLabel, style, buttonInnerColor, onPressHandler }) => {

    return (
        <View style={{ height: 40 }}>
            <RadioButton 
                currentValue={value} 
                value={label} 
                onPress={onPressHandler}
                buttonInnerColor={'#04dce0'}
                buttonStyle={{color: '#04dce0' }}>
                <Text style={style}>{txtLabel}</Text>
            </RadioButton>
        </View>
    )
}

export { Radio };
