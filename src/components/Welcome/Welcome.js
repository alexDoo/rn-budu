import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Welcome = () => {
  return (
    <View style={styles.welcomeWrap}>
      <Text style={styles.instructions}>
        To get on your best trip down the road of joy
      </Text>
      <Text style={styles.instructions}>
        Hit this nice neat button down below
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  welcomeWrap: {
    marginTop: 20,
    padding: 4,
    backgroundColor: '#04dce0',
    borderColor: '#04dce0',
    borderWidth: 1,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    elevation: 10
  },
  instructions: {
    fontSize: 16,
    textAlign: 'center',
    color: '#333',
    marginBottom: 6,
    color: '#fff'
  },
});

export default Welcome;
