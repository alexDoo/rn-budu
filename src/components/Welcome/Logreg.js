import React, { Component } from 'react';
import { View, Text, StyleSheet, Button as RNButton, Alert } from 'react-native';
import { FormLabel, FormInput, FormValidationMessage, Button  } from 'react-native-elements';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { 
  logregUpdate,
  handleFlow,
  addUser,
  loginUser
} from '../../actions';

import { Radio, Spinner } from '../common';

class LogregComponent extends Component {

  componentWillMount() {
    if (this.props.currentUser) {
      // redirect a user home/profile
      Actions.profileEdit();
    }
  }

  doRegister() {
    if (Object.values(this.props.error).join('').length < 1) {
      return this.props.addUser(this.props);
    }

    return false;
  }

  doLogin() {
    if (Object.values(this.props.error).join('').length < 1) {
          return this.props.loginUser(this.props);
    }

    return false;
  }

  _renderName() {
    if (this.props.flow === 'register') {
        return (
            <View style={styles.input}>
                <FormLabel>Name</FormLabel>
                <FormInput
                    value={this.props.name}
                    autocorrect={false}
                    maxLength={40}
                    onChangeText={value => this.props.logregUpdate({ prop: 'name', value })}
                />
            </View>
      );
    } else {
      return null;
    }
  }

  _renderGender() {
    if (this.props.flow === 'register') {
      return (
        <View style={styles.radio}>
          <Radio
            value={this.props.gender}
            label={'male'}
            txtLabel={'Male'}
            onPressHandler={value => this.props.logregUpdate({ prop: 'gender', value })}
            style={styles.radiotxt}
            buttonInnerColor="#04dce0"
          />
          <Radio
            value={this.props.gender}
            label="female"
            txtLabel="Female"
            onPressHandler={value => this.props.logregUpdate({ prop: 'gender', value })}
            style={styles.radiotxt}
            buttonInnerColor="#04dce0"
          />
      </View>
      );
    } else {
      return null;
    }  
  }

  _renderButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button
          raised
          icon={{ name: 'perm-identity' }}
          onPress={this.props.flow === 'register' ? () => this.doRegister() : () => this.doLogin()}
          title={this.props.flow === 'register' ? 'JOIN THE CLUB' : 'LOGIN'}
        />
    );
  }

  _renderErrors() {
    const err = Object.values(this.props.error).filter(err => {
      if (err && err.length) {
        return err;
      }
    });

    return err.join(', ');
  }

 render() {
   return (
    <View style={styles.register}>
      <View  style={styles.input}>
        <FormLabel>Email</FormLabel>
        <FormInput
            value={this.props.email}
            autocorrect={false}
            maxLength={120}
            onChangeText={value => this.props.logregUpdate({ prop: 'email', value })}
            label={'Email'}
        />
      </View>

      <View  style={styles.input}>
        <FormLabel>Password</FormLabel>
        <FormInput
            value={this.props.password}
            secureTextEntry={true}
            autocorrect={false}
            maxLength={200}
            onChangeText={value => this.props.logregUpdate({ prop: 'password', value })}
            label={'Password'}
        />
      </View>  

      {this._renderName()}

      {this._renderGender()}

      <View style={styles.submitBtn}>
        {this._renderButton()}

        <RNButton
          onPress={() => this.props.handleFlow(this.props.flow)}
          title={this.props.flow === 'register' ? 'I have account' : 'Register'}
          style={styles.flowBtn}
        />
      </View>

      <View style={styles.error}>
        <Text style={styles.ertxt}>{this._renderErrors()}</Text>
      </View>  
    </View>
    );
  }
}

  const styles = StyleSheet.create({
    register: {
      alignItems: 'flex-start',
      marginTop: 20,
      marginBottom: 20,
      width: '94%',
      height: '80%'
    },
    input: {
      width: '100%'
    },
    radio: {
      flex: 1,
      width: '100%',
      marginTop: 20,
      flexDirection: 'row',
      justifyContent: 'space-around'
    },
    radiotxt: {
      color: 'rgb(116, 129, 141)',
      fontSize: 14,
      fontWeight: "600",
      marginTop: 4,
      marginLeft: 8
    },
    flowBtn: {
      flex: 1,
      justifyContent: 'flex-end',
      borderWidth: 1,
      marginTop: 10,
      color: 'silver',
      elevation: 4
    },
    submitBtn: {
      marginTop: 20,
      marginBottom: 0,
      width: '100%',
      height: 'auto'
    },
    error: {
      marginTop: 10,
      marginBottom: 10,
      height: 'auto',
      minHeight: 20
    },
    ertxt: {
      fontSize: 18,
      color: 'red'
    }
  });

  const mapStateToProps = ({ auth, user }) => {
    const { email, password, name, gender, error, logreg_error, loading, flow } = auth;
    const { currentUser } = user;
    
    return { email, password, name, gender, error, logreg_error, loading, flow, currentUser };
  }

  export default connect(mapStateToProps, {
    logregUpdate,
    handleFlow,
    addUser,
    loginUser
  })(LogregComponent);
