import React, { Component } from 'react';
import { ScrollView, Text, StyleSheet, Animated } from 'react-native';
import { Header, Card, Button } from 'react-native-elements';
import { NavigationBar, Title } from '@shoutem/ui';

import Welcome from './Welcome';
import WelcomeImages from './WelcomeImages';
import Register from './Logreg';

import Profile from '../Profile/profileEdit';

export default class EntryComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      backflip: false
    };
  }

  componentWillMount() {
    this.animatedValue = new Animated.Value(0);
    this.value = 0;
    this.animatedValue.addListener(({ value }) => {
      this.value = value;
    });
    this.frontInterpolate = this.animatedValue.interpolate({
      inputRange: [0, 180],
      outputRange: ['0deg', '180deg'],
    });
    this.backInterpolate = this.animatedValue.interpolate({
      inputRange: [0, 180],
      outputRange: ['180deg', '0deg']
    });
  }

  flipToForm() {
    this.setState({ backflip: true });
    Animated.spring(this.animatedValue, {
      toValue: 180,
      friction: 8,
      tension: 10
    }).start();
  }

  render() {
    const frontAnimatedStyle = {
      transform: [
        { rotateY: this.frontInterpolate }
      ]
    };
    const backAnimatedStyle = {
      transform: [
        { rotateY: this.backInterpolate }
      ]
    };
    const frontOpacity = (this.state && this.state.backflip) ?
    { opacity: 0, height: 0 } :
    { opacity: 100, height: 'auto' };
    const backOpacity = (this.state && this.state.backflip) ?
    { opacity: 100, height: 'auto' } :
    { opacity: 0, height:0 };

    return (
      <ScrollView>
        <NavigationBar
          centerComponent={<Title>BUDU LOUNGE</Title>}
        />
        <Card 
          title={(this.state && !this.state.backflip) ? "Welcome" : "Instantiate yourself"}
          titleStyle={{ fontWeight: '200', fontSize: 18 }}
          containerStyle={[styles.container, { justifyContent: 'center', height: 'auto' }]}>
          <Animated.View style={[frontAnimatedStyle, styles.flips, frontOpacity]}>
            <Welcome />
            <WelcomeImages />
            <Button 
              raised
              onPress={() => this.flipToForm()} 
              title={'Login / Register'}
              icon={{ name:'perm-identity' }}
              backgroundColor='#04dce0'
              underlayColor='#80e0e2'
            />
          </Animated.View>
          <Animated.View style={[backAnimatedStyle, styles.flips, styles.backFlip, backOpacity]}>
            <Register />
          </Animated.View>
        </Card>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      marginTop: 80
   },
  contentContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '88%',
    height: '80%',
    marginTop: '15%',
    marginBottom: '15%',
    paddingTop: '26%',
    backgroundColor: '#D5D6CC',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    borderWidth: 2,
    borderColor: '#CACBBF',
    elevation: 20,
  },
  flips: {
    width: '100%',
    backfaceVisibility: 'hidden',
  },
  backFlip: {
    
  },
});
