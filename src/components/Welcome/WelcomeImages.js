import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Alert } from 'react-native';

export default class ImagesComponent extends Component {
  constructor() {
    super();

    this.state = { images: [] };
  }

  componentWillMount() {
    fetch(`https://rallycoding.herokuapp.com/api/music_albums`)
    .then(res => res.json())
    .then(res => {
        this.setState({
        images: res,
      });
    })
    .catch(err => Alert.alert('Network failure', err.message));
  }

  renderImages() {
    return this.state.images.length ?
      this.state.images.map(
        img => <Image 
                key={img.title}
                source={{ uri: img.thumbnail_image }}
                style={styles.anImageHome}
                resizeMode={Image.resizeMode.stretch}
               />
      ) : <Text>loading..</Text>;
  }

  render() {
    return (
      <View style={styles.imagesHome}>
        {this.renderImages()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imagesHome: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 40,
    marginBottom: 20,
    minHeight: 120,
    width: '100%',
  },
  anImageHome: {
    width: 62,
    height: 72,
    alignSelf: 'center',
  },
});
