import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

export default function(ComposedComponent) {
  class Auth extends Component {
    componentWillMount() {
      if (!this.props.currentUser) {
        console.log('NO CURRENT USER', this.props)
        return Actions.welcome();
      }
    }

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  function mapStateToProps({ user }) {
    const { currentUser } = user;

    return { currentUser };
  }

  return connect(mapStateToProps)(Auth);
}
