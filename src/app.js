import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import * as firebase from 'firebase';
import reducers from './reducers';

import Router from './routes';

class App extends Component {
    componentWillMount() {
         // Initialize Firebase
        firebase.initializeApp({
            apiKey: "AIzaSyCeelf9vHEhyjjUpLLHalcRRadxzDh_WJI",
            authDomain: "teezdom.firebaseapp.com",
            databaseURL: "https://teezdom.firebaseio.com",
            projectId: "teezdom",
            storageBucket: "teezdom.appspot.com",
            messagingSenderId: "845582186139"
        });
    }

    render() {
        const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

        return (
            <Provider store={store}>
                <Router />
            </Provider>
        );
    }
}

export default App;
